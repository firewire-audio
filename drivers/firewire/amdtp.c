/*
 * IEC 61883-6 (AMDTP) streaming routines
 *
 * Copyright (C) 2009 Mircea Gherzan <mgherzan@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/kernel.h>

#include <sound/pcm.h>

#include "audio.h"


static int fwa_amdtp_init(struct fwa_stream *s)
{
	struct fwa_amdtp_stream *amdtp_s = (struct fwa_amdtp_stream *)s;
	int events = fraction_ceil(&s->samples_per_cycle);
	int delay = AMDTP_TRANSFER_DELAY;

	/* add the size of the CIP header 
	 * one quadlet per sample per channel
	 */
	s->iso_payload_max = 8 + s->conf.channels * events * 4;

	switch (s->conf.rate) {
	case 44100:
		amdtp_s->fdf = AMDTP_FDF_SFC_44100;
		amdtp_s->syt_interval = 8;
		break;
	case 48000:
		amdtp_s->fdf = AMDTP_FDF_SFC_48000;
		amdtp_s->syt_interval = 8;
		break;
	case 96000:
		amdtp_s->fdf = AMDTP_FDF_SFC_96000;
		amdtp_s->syt_interval = 16;
		break;
	case 192000:
		amdtp_s->fdf = AMDTP_FDF_SFC_192000;
		amdtp_s->syt_interval = 32;
		break;
	default:
		return -EINVAL;
	}

	fraction_init(&s->ticks_per_syt_offset, 
			FWA_ISO_TICK_FREQ * amdtp_s->syt_interval,
			s->conf.rate);

	fraction_init(&s->cycle_offset, (delay % 3072) * s->conf.rate,
			s->conf.rate);

	atomic_set(&s->cycles, delay / 3072);
	atomic_set(&s->cycles_all, 0);

	amdtp_s->dbs = s->conf.channels;

	return 0;
}

/**
 * Compute the next syt value and update internal stream counters
 */
static inline u16 fwa_amdtp_get_syt(struct fwa_amdtp_stream *str, int events)
{
	struct fwa_stream *s = &str->base;
	/* index of the sample for which the timestamp is valid */
	int index = (str->syt_interval - str->dbc) & (str->syt_interval - 1);
	u16 syt;

	if (index < events) {
		syt = ((atomic_read(&s->cycles) << 12) | 
			s->cycle_offset.i) & 0xffff;
			
		fraction_add(&s->cycle_offset, &s->cycle_offset,
				&s->ticks_per_syt_offset);

		/* the cycle offset is modulo 3072 */
		atomic_add(s->cycle_offset.i / 3072, &s->cycles);
		s->cycle_offset.i %= 3072;

	} else
		syt = 0xffff;

	return syt;
}

/**
 * Encode 16-bit Little Endian samples from @src to AM824 format in @dst)
 * 
 * @return	number of bytes read from @src
 */
static void fwa_am824_fill_s16le(__be32 *dst, int events, 
				int channels, u8 *src)
{
	u8 *p = src;
	u32 r;
	int i, j;

	for (i = 0; i < events; i++)
		for (j = 0; j < channels; j++) {
			r = (AM824_MBLA_RAW | AM824_MBLA_VBL16) << 24;
			r |= (p[1] << 16) | (p[0] << 8);
			*dst = cpu_to_be32(r);
			dst++;
			p += 2;		
		}
}


/**
 * Encode 16-bit Little Endian samples from @src to AM824 format in @dst)
 *
 * @return	nubmer of bytes read from @src
 */
static void fwa_am824_fill_s24le(__be32 *dst, int events, 
				int  channels, u8 *src)
{
	u8 *p = src;
	u32 r;
	int i,j;

	for (i = 0; i < events; i++)
		for (j = 0; j < channels; j++) {
			r = (AM824_MBLA_RAW | AM824_MBLA_VBL24) << 24;
			r |= (p[2] << 16) | (p[1] << 8) | p[0];
			*dst = cpu_to_be32(r);
			dst++;
			p += 3;
		}
}

/**
 * Encode data into a CIP packet
 * 
 * @param src	ALSA sample buffer
 * @return	number of bytes read from the ALSA buffer
 */
static int fwa_amdtp_encode(struct fwa_stream *stream, void *src)
{
	struct fwa_amdtp_stream *s = (struct fwa_amdtp_stream *)stream;
	struct fwa_packet *packet;
	struct cip *cip;
	int events, retval = 0;
	u16 syt;

	fraction_add(&stream->ready_samples, &stream->ready_samples, 
			&stream->samples_per_cycle);

	events = stream->ready_samples.i;

	fraction_sub_int(&stream->ready_samples, &stream->ready_samples,
			events);

	packet = &stream->packet_fifo[stream->packet_fifo_head];
	
	cip = (struct cip *)packet->payload;

	syt = fwa_amdtp_get_syt(s, events);

	/* fill the header */
	
	/* first quadlet */
	cip->eoh0	= 0;
	cip->sid	= stream->owner->fw_dev->card->node_id & 0x3f;
	cip->dbs	= s->dbs;
	cip->fn		= 0;
	cip->qpc	= 0;
	cip->sph	= 0;
	cip->reserved	= 0;
	cip->dbc	= s->dbc;

	/* second quadlet */
	cip->eoh1	= 2;
	cip->fmt	= AMDTP_FMT;
	cip->fdf	= s->fdf;
	cip->syt	= cpu_to_be16(syt);

	/* fill the payload */
	switch (stream->substream->runtime->format) {
	case (SNDRV_PCM_FORMAT_S16_LE):
		fwa_am824_fill_s16le(cip->data, events,	stream->conf.channels,
					src);
		retval = events * stream->conf.channels * 2;
 		break;
	case (SNDRV_PCM_FORMAT_S24_LE):
		fwa_am824_fill_s24le(cip->data, events,	stream->conf.channels, 
					src);
		retval = events * stream->conf.channels * 3;
		break;			
	}

	s->dbc += events;

	return retval;
}

/**
 * Decode 16-bit Little Endian samples from @src
 *
 * @return 	number of bytes written to @dst
 */
static void fwa_am824_get_s16le(__be32 *src, int events, int channels,
				u8 *dst)
{
	u8 *p = dst;
	u32 val;
	int i, j;
	
	for (i = 0; i < events; i++)
		for (j = 0; j < channels; j++) {
			val = be32_to_cpu(*src);
			p[1] = (val >> 16) & 0xff;
			p[0] = (val >> 8)  & 0xff;
			src++;
			p += 2;
		
		}
}

/**
 * Decode 24-bit Little Endian samples from @src
 *
 * @return 	number of bytes written to @dst
 */
static void fwa_am824_get_s24le(__be32 *src, int events, int channels,
				u8 *dst)
{
	u8 *p = dst;
	u32 val;
	int i, j;
	
	for (i = 0; i < events; i++)
		for (j = 0; j < channels; j++) {
			val = be32_to_cpu(*src);
			p[2] = (val >> 16) & 0xff;
			p[1] = (val >> 8)  & 0xff;
			p[0] = (val)       & 0xff;
			src++;
			p += 3;	
		}
}

/**
 * Decode data from a CIP packet
 * 
 * @param dst	ALSA sample buffer
 * @return 	number of bytes written to the ALSA buffer
 */
static int fwa_amdtp_decode(struct fwa_stream *stream, void *dst)
{
	//struct fwa_amdtp_stream *s = (struct fwa_amdtp_stream *)stream;
	struct fwa_packet *packet;
	struct cip *cip;
	int events, retval = 0;

	packet = &stream->packet_fifo[stream->packet_fifo_tail];

	events = (packet->iso.payload_length - sizeof(struct cip)) /
		 stream->conf.channels;

	cip = (struct cip *)packet->payload;

	switch(stream->substream->runtime->format) {
	case SNDRV_PCM_FORMAT_S16_LE:
		events /= 2;
		fwa_am824_get_s16le(cip->data, events, stream->conf.channels,
					dst);
		retval = events * stream->conf.channels * 2;
		break;
	case SNDRV_PCM_FORMAT_S24_LE:
		events /= 3;
		fwa_am824_get_s24le(cip->data, events, stream->conf.channels,
					dst);	
		retval = events * stream->conf.channels * 3;	
		break;
	}

	return retval;
}

struct fwa_stream_ops fwa_amdtp_stream_ops = {
	.init		= fwa_amdtp_init,
	.encode		= fwa_amdtp_encode,
	.decode		= fwa_amdtp_decode
};
