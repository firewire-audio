/*
 * FireWire audio (and music data) streaming driver structures
 *
 * Copyright (C) 2009 Mircea Gherzan <mgherzan@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef _FIREWIRE_AUDIO_H
#define _FIREWIRE_AUDIO_H

#include <linux/types.h>
#include <linux/firewire/audio.h>
#include <linux/firewire.h>
#include <linux/cdev.h>
#include <linux/spinlock.h>

#include <sound/core.h>
#include <sound/pcm.h>

#define _DEBUG
#ifdef _DEBUG
#define dprintk(...) 					\
	do {						\
		printk(KERN_NOTICE "%s(): ", __func__);	\
		printk(__VA_ARGS__);			\
		printk("\n");				\
	} while(0)
#else
#define dprintk(...) do {} while(0)
#endif

/* ALSA related */

/* iso transfer */
#define	FWA_ISO_PAGE_COUNT	8
#define FWA_ISO_TICK_FREQ	24576000

/* userspace configuration */
#define FWA_CDEV_MAJOR		300
#define FWA_CDEV_MAX_MINORS	8

struct fwa_device;
struct fwa_stream;
struct fwa_stream_ops;

struct fwa_amdtp_stream;
struct fwa_amdtp_stream_ops;


/* requiread for 44100Hz operation */
struct fraction {
	int i;		/* integer part (floor) */
	int n;		/* numerator */
	int d;		/* denominator */
};

/* view of the ALSA DMA buffer */
struct fwa_buffer {
	char 		*data;

	unsigned int 	size;
	/* last "hardware" position */
	unsigned int 	tail;

	spinlock_t	lock;
};


struct fwa_packet {
	/* index within the iso_buffer */
	unsigned int 		payload_index;
	/* virtual address of the payload */
	void 			*payload;
	
	struct fw_iso_packet	iso;	
};

struct fwa_stream {
	/* fw_audioX file for userspace configuration */
	struct cdev		cdev;
	
	struct snd_pcm_substream *substream;
	
	struct fwa_stream_conf 	conf;

	struct fwa_device 	*owner;

	struct fwa_buffer 	alsa_buffer;

	struct fwa_packet	*packet_fifo;
	unsigned int 		packet_fifo_size;
	unsigned int 		packet_fifo_head;
	unsigned int 		packet_fifo_tail;

	struct fraction		samples_per_cycle;
	struct fraction		ready_samples;
	
	struct fraction 	cycle_offset;
	/* ticks running at 24.576MHz */
	struct fraction		ticks_per_syt_offset;

	atomic_t		cycles;
	atomic_t		cycles_all;

	/* largest payload size (CIP/etc header included) */
	unsigned int		iso_payload_max;
	
	/* does NOT include the iso header size */
	unsigned int 		header_size;

	struct fw_iso_context 	*iso_ctx;
	struct fw_iso_buffer 	iso_buffer;
};

/**
 * The audio device aggregating all the physical interfaces (units)
 * that have the same card as a parent
 */
struct fwa_device {
	struct cdev 		cdev;

	struct fwa_dev_conf 	conf;
	
	struct fwa_stream 	*stream_play;
	struct fwa_stream 	*stream_cap;
	struct fwa_stream_ops	*ops;

	struct fw_device 	*fw_dev;

	struct snd_card 	*snd_card;
};

/**
 * Streaming format abstracion (AMDTP, DICE, etc).
 */
struct fwa_stream_ops {
	int (*init)(struct fwa_stream *stream);
	
	int (*encode)(struct fwa_stream *stream, void *dst);
	
	int (*decode)(struct fwa_stream *stream, void *src); 
};


/**
 * Common Isochronous Packet 
 * Taken from the AMDTP OSS driver
 */
#if defined __BIG_ENDIAN_BITFIELD

struct cip {
	/* first quadlet */
	unsigned int dbs	: 8;
	unsigned int eoh0	: 2;
	unsigned int sid	: 6;

	unsigned int dbc	: 8;
	unsigned int fn		: 2;
	unsigned int qpc	: 3;
	unsigned int sph	: 1;
	unsigned int reserved	: 2;

	/* second quadlet */
	unsigned int fdf      	: 8;
	unsigned int eoh1	: 2;
	unsigned int fmt      	: 6;

	unsigned int syt      	: 16;

	/* payload quadlets */
        __be32 data[0];
};

#elif defined __LITTLE_ENDIAN_BITFIELD

struct cip {
	/* first quadlet */
	unsigned int sid	: 6;
	unsigned int eoh0	: 2;
	unsigned int dbs	: 8;

	unsigned int reserved	: 2;
	unsigned int sph	: 1;
	unsigned int qpc	: 3;
	unsigned int fn		: 2;
	unsigned int dbc	: 8;

	/* second quadlet */
	unsigned int fmt	: 6;
	unsigned int eoh1	: 2;
	unsigned int fdf	: 8;

	unsigned int syt	: 16;

	/* payload quadlets */
	__be32 data[0];
};

#else

#error Unknown bitfield type

#endif



#define AMDTP_TRANSFER_DELAY	9000

/* CIP header constants */
#define AMDTP_FMT		0x10
#define AMDTP_FDF_SFC_44100	1
#define AMDTP_FDF_SFC_48000	2
#define AMDTP_FDF_SFC_96000	4
#define AMDTP_FDF_SFC_192000	6

/* AM824 labels */
#define AM824_MBLA_RAW		0x40 
#define AM824_MBLA_VBL24	0
#define AM824_MBLA_VBL20	1
#define AM824_MBLA_VBL16	2

struct fwa_amdtp_stream {
	struct fwa_stream base;

	u8 fdf;
	u8 dbc;
	u8 dbs;
		
	unsigned int syt_interval;
};


extern struct fwa_stream_ops fwa_amdtp_stream_ops;

static inline void fraction_init(struct fraction *f, int num, int denom)
{
	f->i = num / denom;
	f->n = num % denom;
	f->d = denom;
}


/**
 * Add to fractions with the same denominator
 */
static inline void fraction_add(struct fraction *sum, struct fraction *f1,
				struct fraction *f2)
{
	int s = f1->n + f2->n;

	sum->i = f1->i + f2->i + s/(f1->d);
	sum->n = s % f1->d;
	sum->d = f1->d;
}

static inline int fraction_ceil(struct fraction *f)
{
	return (f->n ? 1 : 0) + f->i;
}

/**
 * Substract an integer from a fraction
 */
static inline void fraction_sub_int(struct fraction *diff, 
				    struct fraction *f, int integer)
{
	diff->i = f->i - integer;
	diff->n = f->n;
	diff->d = f->d;
}

static inline u64 fw_guid(struct fw_device *dev)
{
	return ((u64)dev->config_rom[3] << 32 | dev->config_rom[4]);
}


/**
 * Convert a rate to its corresponding ALSA constant
 */
static inline unsigned int pcm_sndrv_rate(__u32 numerical)
{
	switch(numerical) {
	case 8000:	return SNDRV_PCM_RATE_8000;
	case 44100:	return SNDRV_PCM_RATE_44100;
	case 48000:	return SNDRV_PCM_RATE_48000;
	case 96000:	return SNDRV_PCM_RATE_96000;
	case 192000:	return SNDRV_PCM_RATE_192000;
	}

	return 0; 
}

#endif /* _FIREWIRE_AUDIO_H */
