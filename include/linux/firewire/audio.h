/*
 * Userspace interface for configuring the FireWire audio streaming
 *
 * Copyright (C) 2009 Mircea Gherzan <mgherzan@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef _LINUX_FIREWIRE_AUDIO_H
#define _LINUX_FIREWIRE_AUDIO_H

/* stream formats/types */
#define FWA_FORMAT_AMDTP		1

/**
 * Configuration data for the whole device
 */
struct fwa_dev_conf {
	/* see FWA_TYPE_ above */
	__u8 format;
	__u8 streams_cap;
	__u8 streams_play;
};

/**
 * Configuration data for streams
 */
struct fwa_stream_conf {
	/* stream index in the fwa_device */
	__u8 index;

	__u8 channels;	

	/* isochronous channel index */
	__u8 iso_channel;
	
	/* in the ALSA _FMTBIT_ format */
	__u64 format;

	/* rate value, not the ALSA constant */
	__u32 rate;
};


/* starting a new block in the FireWire ioctl space */
#define FWA_IOCTL_CONFIG_DEV	_IOW('#', 0x30, struct fwa_dev_conf)	
#define FWA_IOCTL_CONFIG_PLAY	_IOW('#', 0x31, struct fwa_stream_conf)
#define FWA_IOCTL_CONFIG_CAP	_IOW('#', 0x32, struct fwa_stream_conf)

#endif /* _LINUX_FIREWIRE_AUDIO_H */
