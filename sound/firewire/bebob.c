/*
 * Focusrite Saffire LE driver
 *
 * Copyright (c) Mircea Gherzan <mgherzan@gmail.com>
 * Licensed under the terms of the GNU General Public License, version 2.
 */

#include <linux/module.h>
#include <linux/mod_devicetable.h>
#include <linux/device.h>
#include <linux/firewire.h>
#include <sound/core.h>
#include <sound/pcm.h>

#define FOCUSRITE_VENDOR_ID	0x00130e
#define SAFFIRE_LE_MODEL_ID	0x000000

struct bebob {
	struct fw_unit			*unit;
	struct snd_card			*snd_card;
	struct snd_pcm_substream	*pcm_out;
	struct amdtp_out_stream		*amdtp_out;
	struct fw_iso_resources		*iso_res_out;
};


static int param_debug = 1;
module_param_named(debug, param_debug, int, 0644);
MODULE_PARM_DESC(debug, "Debug output (default = 0)");

#define dprintk(...)						\
	do {							\
		if (unlikely(param_debug)) {			\
			printk(KERN_INFO KBUILD_MODNAME ": ");	\
			printk(__VA_ARGS__);			\
			printk("\n");				\
		}						\
	} while(0)


static int bebob_pcm_open(struct snd_pcm_substream *sub)
{
	struct bebob *bob = sub->private_data;
	struct snd_pcm_runtime *runtime = sub->runtime;

	dprintk("pcm open, runtime %p", runtime);

	return 0;
}

static int bebob_pcm_close(struct snd_pcm_substream *sub)
{
	struct bebob *bob = sub->private_data;

	dprintk("pcm close");

	return 0;
}

static int bebob_pcm_prepare(struct snd_pcm_substream *sub)
{
	struct bebob *bob = sub->private_data;

	dprintk("pcm prepare");

	return 0;
}


static int bebob_pcm_trigger(struct snd_pcm_substream *sub, int cmd)
{
	struct bebob *bob = sub->private_data;
	struct snd_pcm_substream *new_sub;

	switch (cmd) {
	case SNDRV_PCM_TRIGGER_START:
		new_sub = sub;
		dprintk("substream start");
		break;
	case SNDRV_PCM_TRIGGER_STOP:
		new_sub = NULL;
		dprintk("substream stop");
		break;
	default:
		return -EINVAL;
	}

	//amdtp_out_stream_pcm_trigger(&bebop->amdtp_out, current)
	return 0;
}

static snd_pcm_uframes_t bebob_pcm_pointer(struct snd_pcm_substream *sub)
{
	struct bebob *bob = sub->private_data;

	dprintk("pcm pointer");

	return 0;
}

static int bebob_pcm_hw_params(struct snd_pcm_substream *sub,
			       struct snd_pcm_hw_params *hw)
{
	struct bebob *bob = sub->private_data;

	dprintk("hw params");

	return 0;
}

static int bebob_pcm_hw_free(struct snd_pcm_substream *sub)
{
	struct bebob *bob = sub->private_data;

	dprintk("hw free");

	return 0;
}

static struct snd_pcm_ops bebob_pcm_ops = {
	.open		= bebob_pcm_open,
	.close		= bebob_pcm_close,
	.prepare	= bebob_pcm_prepare,
	.trigger	= bebob_pcm_trigger,
	.pointer	= bebob_pcm_pointer,
	.hw_params	= bebob_pcm_hw_params,
	.hw_free	= bebob_pcm_hw_free,
	.ioctl		= snd_pcm_lib_ioctl,
	.page		= snd_pcm_lib_get_vmalloc_page,
	.mmap		= snd_pcm_lib_mmap_vmalloc
};


static int bebob_init_snd(struct bebob *bob, struct snd_card *card)
{
	struct fw_device *dev = fw_parent_device(bob->unit);
	struct snd_pcm *pcm;
	char model[32]  = "Unknown model";
	char vendor[16] = "Unknwon vendor";
	int ret;

	fw_csr_string(dev->config_rom + 5, CSR_VENDOR, vendor, sizeof(vendor));
	fw_csr_string(bob->unit->directory, CSR_MODEL, model, sizeof(model));

	dprintk("setting driver name");
	strcpy(card->driver, "BeBob");
	dprintk("setting short name");
	strcpy(card->shortname, model);
	snprintf(card->longname, sizeof(card->longname), "%s %s at %s",
		 vendor, model, dev_name(&bob->unit->device));

	dprintk("creating new pcm");

	ret = snd_pcm_new(card, "BeBoB", 0, 1, 0, &pcm);
	if (ret)
		return ret;

	strcpy(pcm->name, card->shortname);
	pcm->private_data = bob;

	bob->snd_card = card;
	bob->pcm_out = pcm->streams[SNDRV_PCM_STREAM_PLAYBACK].substream;
	bob->pcm_out->private_data = bob;
	bob->pcm_out->ops = &bebob_pcm_ops;

	return 0;
}

static int bebob_init_fw(struct bebob *bob)
{

	dprintk("init firewire resources");

	return 0;
}

static void bebob_release_fw(struct bebob *bob)
{

	dprintk("releasing firewire resources");

	return 0;
}

static int __devinit bebob_probe(struct device *dev)
{
	struct snd_card *card;
	struct bebob *bob;
	int ret;

	dprintk("probing");

	ret = snd_card_create(-1, NULL, THIS_MODULE, sizeof(*bob), &card);
	if (ret < 0)
		return ret;

	bob = card->private_data;
	bob->unit = fw_unit_get(fw_unit(dev));
	fw_device_get(fw_parent_device(bob->unit));

	ret = bebob_init_snd(bob, card);
	if (ret)
		goto err_init;

	ret = bebob_init_fw(bob);
	if (ret)
		goto err_init;

	ret = snd_card_register(card);
	if (ret)
		goto err_reg;

	dev_set_drvdata(dev, bob);

	return 0;

err_reg:
	bebob_release_fw(bob);
err_init:
	fw_unit_put(bob->unit);
	fw_device_put(fw_parent_device(bob->unit));
	snd_card_free(card);
	return ret;
}

static void bebob_bus_reset(struct fw_unit *unit)
{
	struct bebob *bob = dev_get_drvdata(&unit->device);

	dprintk("bus reset");
}

static int __devexit bebob_remove(struct device *dev)
{
	struct bebob *bob = dev_get_drvdata(dev);

	dprintk("bebob remove");

	snd_card_free(bob->snd_card);

	return 0;
}

static const struct ieee1394_device_id bebob_id_table[] = {
	{
		.match_flags	= IEEE1394_MATCH_VENDOR_ID |
				  IEEE1394_MATCH_MODEL_ID,
		.vendor_id	= FOCUSRITE_VENDOR_ID,
		.model_id	= SAFFIRE_LE_MODEL_ID
	},
	{}
};

static struct fw_driver bebob_driver = {
	.driver	= {
		.owner	= THIS_MODULE,
		.name	= KBUILD_MODNAME,
		.bus	= &fw_bus_type,
		.probe	= bebob_probe,
		.remove = __devexit_p(bebob_remove),
	},
	.update	  = bebob_bus_reset,
	.id_table = bebob_id_table,
};

static int __init snd_bebob_init(void)
{
	dprintk("-----------------------");
	return driver_register(&bebob_driver.driver);
}

static void __exit snd_bebob_exit(void)
{
	driver_unregister(&bebob_driver.driver);
	dprintk("unloaded");
}

module_init(snd_bebob_init);
module_exit(snd_bebob_exit);

MODULE_DESCRIPTION("BridgeCo BeBoB driver");
MODULE_AUTHOR("Mircea Gherzan <mgherzan@gmail.com>");
MODULE_LICENSE("GPL v2");
MODULE_DEVICE_TABLE(ieee1394, bebob_id_table);
