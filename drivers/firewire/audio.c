/*
 * FireWire audio (and music data) streaming driver
 *
 * Copyright (C) 2009 Mircea Gherzan <mgherzan@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/firewire.h>
#include <linux/firewire-constants.h>
#include <linux/mod_devicetable.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/dma-mapping.h>
#include <linux/highmem.h>
#include <linux/spinlock.h>

#include <asm/page.h>

#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/initval.h>

#include <linux/firewire/audio.h>

#include "core.h"
#include "audio.h"

/* standard module options for ALSA */
static int snd_index[SNDRV_CARDS] 	= SNDRV_DEFAULT_IDX;
static char *snd_id[SNDRV_CARDS] 	= SNDRV_DEFAULT_STR;
static int snd_enable[SNDRV_CARDS] 	= SNDRV_DEFAULT_ENABLE_PNP;


static void fwa_update(struct fw_unit *unit)
{
	struct fw_device *dev = fw_parent_device(unit);
	
	dprintk("device GUID %016llx", fw_guid(dev));
}


/* tasklet context, called once per iso cycle */
static void fwa_iso_cb(struct fw_iso_context *ctx, u32 cycle, 
			size_t header_len, void *header, void *data)
{
	struct fwa_stream *s = data;
	struct fwa_device *dev = s->owner;
	struct fwa_packet *p;
	struct fw_card *card = dev->fw_dev->card;
	int bytes, err;

	dprintk("cycle %x header_len %lu header %p data %p", cycle, header_len,
		header, data);

	/* on recording, copy the data in the ALSA buffer beforse signaling 
	 * that a period has elapsed
	 */
	if (s->substream->runtime->format == SNDRV_PCM_STREAM_CAPTURE) {
		spin_lock(&s->alsa_buffer.lock);
		bytes = dev->ops->decode(s, s->alsa_buffer.data + 
					s->alsa_buffer.tail);
		s->alsa_buffer.tail += bytes;
		s->alsa_buffer.tail %= s->alsa_buffer.size;	
		spin_unlock(&s->alsa_buffer.lock);
	}

	snd_pcm_period_elapsed(s->substream);

	s->packet_fifo_tail++;
	s->packet_fifo_tail %= s->packet_fifo_size;

	if (s->substream->runtime->format == SNDRV_PCM_STREAM_PLAYBACK) {
		spin_lock(&s->alsa_buffer.lock);
		bytes = dev->ops->encode(s, s->alsa_buffer.data + 
					s->alsa_buffer.tail);
		s->alsa_buffer.tail += bytes;
		s->alsa_buffer.tail %= s->alsa_buffer.size;

		spin_unlock(&s->alsa_buffer.lock);
	}
	
	
	p = &s->packet_fifo[s->packet_fifo_head];
	p->iso.interrupt = 1;
	p->iso.skip = 0;
	p->iso.tag = 1;
	p->iso.sy = 0;

	err = card->driver->queue_iso(s->iso_ctx, &p->iso, &s->iso_buffer,
					p->payload_index);
	if (unlikely(err < 0)) {
		fw_error("unable to queue packet, err %d\n", err);
		return;
	}

	s->packet_fifo_head++;
}

/**
 * Compute the bandwidth required for a stream,
 * in "bandwidth allocation units" (20ns)
 */
static inline int fwa_stream_bandwidth(struct fwa_stream *s)
{
	/* in kilobits/second */
	int bw = s->iso_payload_max * 8 * 8;

	/* at S400, using one unit => 64 kilobits/second */
	return DIV_ROUND_UP(bw, 64);
}

/**
 * Allocate and initialize the FireWire streaming structure
 */
static int fwa_stream_init(struct fwa_stream *s)
{
	struct fwa_device *dev = s->owner;
	struct fwa_packet *p;
	struct fw_card *card = dev->fw_dev->card;
	unsigned int packet_count, packet_size, i, j, index;
	int err, channel, bandwidth; 
	int iso_dir, dma_dir;
	void *page_logical;
	/* accept only the channel provided by the ioctl */
	u64 ch_mask = 1ULL << s->conf.iso_channel;
	__be32 buffer[2];

	if (s->substream->stream == SNDRV_PCM_STREAM_CAPTURE) {
		iso_dir = FW_ISO_CONTEXT_RECEIVE;
		dma_dir = DMA_FROM_DEVICE;	
	} else {
		iso_dir = FW_ISO_CONTEXT_TRANSMIT;
		dma_dir = DMA_TO_DEVICE;	
	}

	fraction_init(&s->samples_per_cycle, s->conf.rate, 8000);
	fraction_init(&s->ready_samples, 0, 8000);

	err = dev->ops->init(s);
	if (err < 0)
		return err;

	bandwidth = fwa_stream_bandwidth(s);

	/* allocate the channel */
	fw_iso_resource_manage(card, dev->fw_dev->generation, ch_mask,
				&channel, &bandwidth, 1, buffer);
	if ((channel < 0) || !bandwidth) {
		fw_error("cannot allocate channel\n");
		return -EINVAL;
	}

	dprintk("bandwith %d channel %d", bandwidth, channel);

	/* create the context */
	s->iso_ctx = fw_iso_context_create(card, iso_dir, channel, 
					card->link_speed, s->header_size + 8,
					fwa_iso_cb, s);
	if (IS_ERR(s->iso_ctx)) {
		fw_error("cannot create iso context\n");
		err = PTR_ERR(s->iso_ctx);
		goto out_manage;
	}
					
	/* initialize the buffer */
	err = fw_iso_buffer_init(&s->iso_buffer, card, FWA_ISO_PAGE_COUNT,
				dma_dir);
	if (err < 0) {
		fw_error("cannot create iso buffer\n");
		goto out_ctx;
	}

	packet_size  = s->iso_payload_max;
	packet_count = (PAGE_SIZE / packet_size) * FWA_ISO_PAGE_COUNT;

	/* make room for the iso header ("immediate header") */
	s->packet_fifo = kzalloc(packet_count * (8 + sizeof(struct fwa_packet)),
				GFP_KERNEL);
	
	if (!s->packet_fifo) {
		fw_error("failed to allocate packet FIFO\n");
		err = -ENOMEM;
		goto out_buffer;
	}

	index = 0;
	for (i = 0; i < FWA_ISO_PAGE_COUNT; i++) {

		page_logical = kmap(s->iso_buffer.pages[i]);

		for (j = 0; j < PAGE_SIZE / packet_size; j++) {
			p = &s->packet_fifo[index];
			p->payload = (u8*)page_logical + j * packet_size;
			p->payload_index = i * PAGE_SIZE + j * packet_size; 
			index++;
		}
			 
	}

	s->packet_fifo_size = packet_count;

	spin_lock_init(&s->alsa_buffer.lock);

	return 0;

out_buffer:
	fw_iso_buffer_destroy(&s->iso_buffer, card);
out_ctx:
	fw_iso_context_destroy(s->iso_ctx);
out_manage:
	fw_iso_resource_manage(card, dev->fw_dev->generation, ch_mask,
				&channel, &bandwidth, 0, buffer);
	return err;
}


static int fwa_stream_free(struct fwa_stream *stream)
{
	struct fw_device *fw_dev = stream->owner->fw_dev;
	unsigned int i;
	int channel = stream->conf.iso_channel;
	int bandwidth = fwa_stream_bandwidth(stream);
	u64 ch_mask = (1ULL << stream->conf.iso_channel);
	__be32 buffer[2];


	for (i = 0; i < FWA_ISO_PAGE_COUNT; i++)
		kunmap(stream->iso_buffer.pages[i]);

	kfree(stream->packet_fifo);

	dprintk("destroying the iso buffer");
	fw_iso_buffer_destroy(&stream->iso_buffer, fw_dev->card);

	dprintk("destroying the iso context");
	fw_iso_context_destroy(stream->iso_ctx);

	dprintk("deallocating channel and bandwidth");
	fw_iso_resource_manage(fw_dev->card, fw_dev->generation, ch_mask,
				&channel, &bandwidth, 0, buffer);
	if (channel < 0)
		fw_error("error deallocating channel\n");
	
	if (bandwidth == 0)
		fw_error("error deallocating bandwidth\n");

	return 0;
}

static int fwa_stream_start(struct fwa_stream *s)
{
	struct fw_card *card = s->owner->fw_dev->card;
	struct fwa_device *dev = s->owner;
	struct fwa_packet *p;
	int err, done;
	u32 start_cycle;

	start_cycle = card->driver->get_cycle_time(card) + 4;

	err = fw_iso_context_start(s->iso_ctx, start_cycle, 0, 0);
	if (err < 0) {
		fw_error("cannot start iso context");
		return err;
	}

	dprintk("context started on cycle %x", start_cycle);

	s->packet_fifo_head = s->packet_fifo_tail = 0;

	spin_lock(&s->alsa_buffer.lock);
	
	if (s->substream->stream == SNDRV_PCM_STREAM_PLAYBACK) {
		done = dev->ops->encode(s, s->alsa_buffer.data +
					s->alsa_buffer.tail);
		s->alsa_buffer.tail = (s->alsa_buffer.tail + done) % 
					s->alsa_buffer.size;
	}

	spin_unlock(&s->alsa_buffer.lock);

	p = &s->packet_fifo[s->packet_fifo_head];
	p->iso.interrupt = 1;
	p->iso.skip = 0;
	p->iso.tag = 1;
	p->iso.sy = 0;

	err = card->driver->queue_iso(s->iso_ctx, &p->iso, &s->iso_buffer,
					p->payload_index);
	if (unlikely(err < 0)) {
		fw_error("unable to queue packet, err %d\n", err);
		return err;
	}

	s->packet_fifo_head++;

	return 0;
}

static int fwa_stream_stop(struct fwa_stream *s)
{
	
	dprintk("stopping context");
	fw_iso_context_stop(s->iso_ctx);

	return 0;
}

static struct snd_pcm_hardware fwa_pcm_playback_hw = {
	.info 			= SNDRV_PCM_INFO_INTERLEAVED |
                   		  SNDRV_PCM_INFO_BLOCK_TRANSFER,
	.formats 		= SNDRV_PCM_FMTBIT_S16_LE,
	.rates 			= SNDRV_PCM_RATE_8000_192000,
	.rate_min 		= 8000,
	.rate_max 		= 192000,
	.channels_min 		= 2,
	.channels_max 		= 2,
	.buffer_bytes_max 	= 32768,
	.period_bytes_min 	= 4096,
	.period_bytes_max 	= 32768,
	.periods_min 		= 1,
	.periods_max 		= 1024,
};


static struct snd_pcm_hardware fwa_pcm_capture_hw = {
	.info 			= SNDRV_PCM_INFO_INTERLEAVED |
                   		  SNDRV_PCM_INFO_BLOCK_TRANSFER,
	.formats 		= SNDRV_PCM_FMTBIT_S16_LE,
	.rates 			= SNDRV_PCM_RATE_8000_192000,
	.rate_min 		= 8000,
	.rate_max 		= 192000,
	.channels_min 		= 1,
	.channels_max 		= 1,
	.buffer_bytes_max 	= 32768,
	.period_bytes_min 	= 4096,
	.period_bytes_max 	= 32768,
	.periods_min 		= 1,
	.periods_max 		= 1024,
};


static int fwa_pcm_open(struct snd_pcm_substream *substream)
{
	struct fwa_device *dev = snd_pcm_substream_chip(substream);
	struct fwa_stream *stream, *vec;
	struct fwa_stream_conf *conf;
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct snd_pcm_hardware *pcm_hw, *rhw;
	int idx = substream->number;

	dprintk("idx %d, dev GUID %016llx", idx, fw_guid(dev->fw_dev));

	if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK) {
		vec = dev->stream_play;
		pcm_hw = &fwa_pcm_playback_hw;
	}
	else {
		vec = dev->stream_cap;
		pcm_hw = &fwa_pcm_capture_hw;
	}


	if (vec[idx].conf.format == 0) {
		fw_error("stream not configured\n");
		return -ENODEV;
	}

	stream = &vec[idx];
	stream->owner = dev;
	stream->substream = substream;

	runtime->private_data = stream;
	rhw = &runtime->hw;
	*rhw = *pcm_hw;

	conf = &stream->conf;

	/* enforce configuration coming from userspace */
	rhw->channels_min	= conf->channels;
	rhw->channels_max	= conf->channels; 
	rhw->rate_min		= conf->rate;
	rhw->rate_max		= conf->rate;	
	rhw->rates		= pcm_sndrv_rate(conf->rate);
	rhw->formats		= (1ULL << conf->format);
	
	return fwa_stream_init(stream);
}

static int fwa_pcm_close(struct snd_pcm_substream *substream)
{
	return fwa_stream_free(substream->runtime->private_data);
}

static int fwa_pcm_hw_params(struct snd_pcm_substream *substream,
				struct snd_pcm_hw_params *hw_params)
{
	int err;

	dprintk("substream %p", substream);
	
	err = snd_pcm_lib_malloc_pages(substream, 
					params_buffer_bytes(hw_params));

	return err;
}

static int fwa_pcm_hw_free(struct snd_pcm_substream *substream)
{
	dprintk("substream %p", substream);

	return snd_pcm_lib_free_pages(substream);
}


static int fwa_pcm_prepare(struct snd_pcm_substream *substream)
{	
	struct snd_pcm_runtime *runtime = substream->runtime;
	//struct fwa_device *dev = snd_pcm_substream_chip(substream);
	struct fwa_stream *fwa_stream = runtime->private_data;
	struct fwa_buffer *buf = &fwa_stream->alsa_buffer;
	//struct fw_card *card = dev->fw_dev->card;

	dprintk("substream %p", substream);
	
	buf->size = snd_pcm_lib_buffer_bytes(substream);
	buf->data = runtime->dma_area;	
	
	return 0;
}


static int fwa_pcm_trigger(struct snd_pcm_substream *substream, int cmd)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct fwa_stream *stream = runtime->private_data;

	dprintk("substream %p fwa %p, cmd %x", substream, stream, cmd);

	switch(cmd) {
	case SNDRV_PCM_TRIGGER_START:
		fwa_stream_start(stream);
		break;
	case SNDRV_PCM_TRIGGER_STOP:
		fwa_stream_stop(stream);	
		break;
	default:
		return -EINVAL;
	}

	return 0;
}

static snd_pcm_uframes_t fwa_pcm_pointer(struct snd_pcm_substream *sub)
{
	struct fwa_stream *s = sub->runtime->private_data;
	unsigned int pos;
	
	spin_lock(&s->alsa_buffer.lock);
	pos = s->alsa_buffer.tail;
	spin_unlock(&s->alsa_buffer.lock);

	dprintk("hw pos (tail) %d", pos);

	return bytes_to_frames(sub->runtime, pos);
}

static struct snd_pcm_ops fwa_playback_ops = {
	.open		= fwa_pcm_open,
	.close		= fwa_pcm_close,
	.ioctl		= snd_pcm_lib_ioctl,
	.hw_params	= fwa_pcm_hw_params,
	.hw_free	= fwa_pcm_hw_free,
	.prepare	= fwa_pcm_prepare,
	.trigger	= fwa_pcm_trigger,
	.pointer	= fwa_pcm_pointer

};

static struct snd_pcm_ops fwa_capture_ops = {	
	.open		= fwa_pcm_open,
	.close		= fwa_pcm_close,
	.ioctl		= snd_pcm_lib_ioctl,
	.hw_params	= fwa_pcm_hw_params,
	.hw_free	= fwa_pcm_hw_free,
	.prepare	= fwa_pcm_prepare,
	.trigger	= fwa_pcm_trigger,
	.pointer	= fwa_pcm_pointer
};


/**
 * Configures a stream, *before* the pcm substream is opened
 */
static int fwa_ioctl_stream(struct fwa_device *dev, unsigned int cmd, 
				void *buffer)
{
	struct fwa_stream_conf *conf = (struct fwa_stream_conf *)buffer;
	struct fwa_stream *vec, *stream;
	u8 max;

	if (!dev->conf.streams_play && !dev->conf.streams_cap) {
		fw_error("cannot configure streams before card!\n");
		return -ENODEV;
	}

	if (cmd == FWA_IOCTL_CONFIG_PLAY) {
		max = dev->conf.streams_play;
		vec = dev->stream_play;
	} else {
		max = dev->conf.streams_cap;
		vec = dev->stream_cap;
	}

	if (conf->index > max) {
		dprintk("%d greater than %d", conf->index, max);
		return -EINVAL;
	}

	stream = &vec[conf->index];

	stream->conf = *conf;
	
	return 0;
}

static int fwa_ioctl_dev(struct fwa_device *dev, unsigned int cmd,
				void *buffer)
{
	struct fwa_dev_conf *conf = (struct fwa_dev_conf *)buffer;
	struct snd_card *card = dev->snd_card;
	struct snd_pcm *pcm;
	unsigned int size;
	int err = 0;

	dev->conf = *conf;

	switch (dev->conf.format) {
	case FWA_FORMAT_AMDTP: {
		size = sizeof(struct fwa_amdtp_stream);
		dev->ops = &fwa_amdtp_stream_ops;
		break;
	}
	default:
		fw_error("unknown stream type %d!\n", dev->conf.format);
		return -EINVAL;	
	}

	dev->stream_play = kzalloc(size * dev->conf.streams_play, GFP_KERNEL);
	if (!dev->stream_play) {
		err = -ENOMEM;
		goto out_card;
	}

	dev->stream_cap = kzalloc(size * dev->conf.streams_cap, GFP_KERNEL);
	if (!dev->stream_cap) {
		err = -ENOMEM;
		goto out_play;
	}

	err = snd_pcm_new(dev->snd_card, "FW-PCM", 0, dev->conf.streams_play, 
				dev->conf.streams_cap, &pcm);
	if (err)
		goto out_cap;

	pcm->private_data = dev;
	strcpy(pcm->name, "FireWire Audio PCM");
	
	snd_pcm_set_ops(pcm, SNDRV_PCM_STREAM_PLAYBACK, &fwa_playback_ops);
	snd_pcm_set_ops(pcm, SNDRV_PCM_STREAM_CAPTURE, &fwa_capture_ops);

	err = snd_pcm_lib_preallocate_pages_for_all(pcm, 
					SNDRV_DMA_TYPE_CONTINUOUS,	
					snd_dma_continuous_data(GFP_KERNEL), 
					12*1024, 1024*1024);
	if (err)
		goto out_cap;
	
	err = snd_card_register(card);
	if (err)
		goto out_cap;
	
	dprintk("format %d capture %d playback %d", dev->conf.format,
		dev->conf.streams_cap, dev->conf.streams_play);
	return 0;

out_cap:
	kfree(dev->stream_cap);
	dev->stream_cap = NULL;
out_play:
	kfree(dev->stream_play);
	dev->stream_play = NULL;
out_card:
	snd_card_free(card);
	return err;
}

static int fwa_cdev_ioctl(struct inode *inode, struct file *file,
				unsigned int cmd, unsigned long arg)
{
	struct fwa_device *dev = container_of(inode->i_cdev,
						struct fwa_device, cdev);
	char buffer[64];
	int err = 0;

	err = copy_from_user(buffer, (char *) arg, _IOC_SIZE(cmd));
	if (err < 0)
		return err;

	switch (cmd) {
	case FWA_IOCTL_CONFIG_DEV:
		return fwa_ioctl_dev(dev, cmd, buffer);
	case FWA_IOCTL_CONFIG_PLAY:
	case FWA_IOCTL_CONFIG_CAP:
		return fwa_ioctl_stream(dev, cmd, buffer);
	}


	return err;
}

static struct file_operations fwa_fops = {
	.owner	= THIS_MODULE,
	.ioctl	= fwa_cdev_ioctl
};

static void fwa_private_free(struct snd_card *card)
{
	struct fwa_device *dev = card->private_data;

	cdev_del(&dev->cdev);

	if (dev->stream_play)
		kfree(dev->stream_play);

	if (dev->stream_cap)
		kfree(dev->stream_cap);

	dprintk("done");
}

static int __devinit fwa_create(struct snd_card *card, struct device *dev)
{
	struct fw_unit *unit = fw_unit(dev);
	struct fw_device *device = fw_parent_device(unit);
	struct fwa_device *fwa_dev;
	int err;
	
	sprintf(card->longname, "Device GUID %016llx", fw_guid(device));

	fwa_dev = card->private_data;
	fwa_dev->snd_card = card;
	fwa_dev->fw_dev = device;
	
	card->private_free = fwa_private_free;

	dev_set_drvdata(dev, fwa_dev);

	cdev_init(&fwa_dev->cdev, &fwa_fops);
	
	dprintk("creating cdev with minor %d", card->number);
	err = cdev_add(&fwa_dev->cdev, MKDEV(FWA_CDEV_MAJOR, card->number), 1);
	if (err)
		fw_error("unable to add cdev");
	
	return err;
}

static int fwa_probe(struct device *dev)
{
	struct snd_card *card;
	static int idx;
	int err;

	err = snd_card_create(snd_index[idx], snd_id[idx], THIS_MODULE, 
				sizeof(struct fwa_device), &card);
	if (err)
		return err;

	strcpy(card->driver, "FireWire Audio");
	sprintf(card->shortname, "FireWire%d", idx);

	err = fwa_create(card, dev);
	if (err) {
		snd_card_free(card);
		return err;
	}
	
	/* registration is delayed until the CONFIG_DEV ioctl */

	idx++;
	
	return 0;
}

static int fwa_remove(struct device *dev)
{
	struct fwa_device *fwa_dev = dev_get_drvdata(dev);
	struct snd_card *snd_card = fwa_dev->snd_card;

	if (fwa_dev) {
		snd_card_free(snd_card);
		dev_set_drvdata(dev, NULL);
	}

	return 0;
}


#define IEC61883_SPECIFIER_ID		0x00a02d

static const struct ieee1394_device_id fwa_id_table[] = {
	{
		.match_flags	= IEEE1394_MATCH_SPECIFIER_ID,
		.specifier_id	= IEC61883_SPECIFIER_ID,
	},
	{ }
};

static struct fw_driver fwa_driver = {
	.driver = {
		.owner 	= THIS_MODULE,
		.name	= "audio",
		.bus	= &fw_bus_type,
		.probe	= fwa_probe,
		.remove	= fwa_remove,
	},
	.update		= fwa_update,
	.id_table	= fwa_id_table,
};	

static int __init fwa_init(void)
{
	printk(KERN_NOTICE "---------------------------------------------\n");
	register_chrdev_region(MKDEV(FWA_CDEV_MAJOR, 0), FWA_CDEV_MAX_MINORS,
				"fw-audio");

	return driver_register(&fwa_driver.driver);
}


static void __exit fwa_exit(void)
{
	driver_unregister(&fwa_driver.driver);

	unregister_chrdev_region(MKDEV(FWA_CDEV_MAJOR, 0), 
				FWA_CDEV_MAX_MINORS);
	
}

module_init(fwa_init);
module_exit(fwa_exit);

module_param_array(snd_index, int, NULL, 0444);
module_param_array(snd_id, charp, NULL, 0444);
module_param_array(snd_enable, bool, NULL, 0444);

MODULE_DESCRIPTION("AMDTP streaming driver");
MODULE_AUTHOR("Mircea Gherzan <mgherzan@gmail.com");
MODULE_LICENSE("GPL");
MODULE_VERSION(__DATE__ " " __TIME__);
MODULE_DEVICE_TABLE(ieee1394, fwa_id_table);

MODULE_PARM_DESC(snd_index, "Index value, for ALSA.");
MODULE_PARM_DESC(snd_id, "ID string, for ALSA.");
MODULE_PARM_DESC(snd_enable, "Enable control, ALSA.");

